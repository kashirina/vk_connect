//
//  Friend.h
//  VK_Connect
//
//  Created by Artyom Ryzhkov on 25/10/15.
//  Copyright © 2015 Yulia Kashirina. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface Friend : NSManagedObject

@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *userId;
@property (nullable, nonatomic, retain) NSString *dateOfBirth;
@property (nullable, nonatomic, retain) NSString *gender;
@property (nullable, nonatomic, retain) NSString *photoUrl;


+ (nonnull Friend *)createFriendWithId:(nonnull NSString *)userId;

+ (nonnull NSArray *)findFriendsWithOffset:(NSInteger)offset count:(NSInteger)count;

@end

