//
//  ViewController.m
//  VK_Connect
//
//  Created by Artyom Ryzhkov on 24/10/15.
//  Copyright (c) 2015 Yulia Kashirina. All rights reserved.
//

#import "VKCFriendsViewController.h"
#import "VKCFriendTableViewCell.h"
#import "VKCCoreDataManager.h"
#import "VKCServerManager.h"
#import "UIImageView+AFNetworking.h"
#import "Friend.h"

#define WSELF           __weak typeof(self) wself = self

@interface VKCFriendsViewController () <VKCServerManagerDelegate>

@property (weak, nonatomic) IBOutlet UIBarButtonItem *loginButton;
@property (strong, nonatomic) NSMutableArray <Friend *> *friends;

@end

@implementation VKCFriendsViewController

static NSInteger numberFriendsInRequest = 5;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [VKCServerManager sharedManager].delegate = self;
    if ([[VKCServerManager sharedManager] isUserAuthorized]) {
        [self startWorking];
    } else {
        [[VKCServerManager sharedManager] authorize];
    }
}

- (void)dealloc {
    [VKCServerManager sharedManager].delegate = nil;
}

- (IBAction)loginButtonPressed:(id)sender {
    if ([self.loginButton.title isEqualToString:@"Login"]) {
        [[VKCServerManager sharedManager] authorize];
    } else {
        [[VKCServerManager sharedManager] logout];
        [[VKCCoreDataManager sharedManager] removeAllData];
        [self.friends removeAllObjects];
        [self.tableView reloadData];
        [[VKCServerManager sharedManager] authorize];
    }
}

#pragma mark - VKCServerManager

- (void)getFriendsFromServer {
    WSELF;
    [VKCServerManager.sharedManager getFriendsWithOffset:wself.friends.count
                                                   count:numberFriendsInRequest
                                               onSuccess:^(NSArray <Friend *> *friends) {
                                                   [wself.friends addObjectsFromArray:friends];
                                                   [wself.tableView reloadData];
                                                   [wself checkLogin];
                                               }
                                               onFailure:^(NSError *error) {
                                                   NSLog(@"error = %@", error);
                                               }];
}

#pragma mark - VKCServerManagerDelegate

- (void)startWorking {
    
    NSArray *friendsFromDB = [Friend findFriendsWithOffset:0 count:numberFriendsInRequest];
    
    if (friendsFromDB.count > 0) {
        self.friends = friendsFromDB.mutableCopy;
        [self.tableView reloadData];
    } else {
        self.friends = [NSMutableArray array];
        [self getFriendsFromServer];
    }
    [self checkLogin];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.friends.count + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = nil;
    if ([self isLastRowIndexPath:indexPath]) {
        cell = [self loadMoreCell];
    } else {
        cell = [self cellForFriend:self.friends[indexPath.row]];
    }
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if ([self isLastRowIndexPath:indexPath]) {
        NSArray *friendsFromDB = [Friend findFriendsWithOffset:self.friends.count count:numberFriendsInRequest];
        if (friendsFromDB.count > 0) {
            [self.friends addObjectsFromArray:friendsFromDB];
            [self.tableView reloadData];
        } else {
            [self getFriendsFromServer];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self isLastRowIndexPath:indexPath]) {
        return 44.f;
    }
    return 80.f;
}

#pragma mark - private methods

- (UITableViewCell *)loadMoreCell {
    static NSString *identifier = @"LoadMoreCell";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    cell.textLabel.text = @"Загрузить ещё";
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    
    return cell;
}

- (UITableViewCell *)cellForFriend:(Friend *)friend{
    NSString *cellIdentifier = NSStringFromClass([VKCFriendTableViewCell self]);
    VKCFriendTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    cell.nameLabel.text = friend.name;
    cell.genderLabel.text = friend.gender;
    cell.dateLabel.text = friend.dateOfBirth;
    cell.dateOfBirthLabel.hidden = !friend.dateOfBirth;
    
    NSURL *imageUrl = [NSURL URLWithString:friend.photoUrl];
    NSURLRequest *request = [NSURLRequest requestWithURL:imageUrl];
    __weak VKCFriendTableViewCell *weakCell = cell;
    [cell.imageView setImageWithURLRequest:request
                          placeholderImage:nil
                                   success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
                                       weakCell.avatarImageView.image = image;
                                   }
                                   failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
                                       NSLog(@"request = %@ response = %@ error = %@", request, response, error);
                                   }];
    return cell;
}

- (BOOL)isLastRowIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == self.friends.count) {
        return YES;
    }
    return NO;
}

- (void)checkLogin {
    if ([[VKCServerManager sharedManager] isUserAuthorized]) {
        self.loginButton.title = @"Logout";
    } else {
        self.loginButton.title = @"Login";
    }
}

@end
