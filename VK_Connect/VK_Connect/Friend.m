//
//  Friend.m
//  VK_Connect
//
//  Created by Artyom Ryzhkov on 25/10/15.
//  Copyright © 2015 Yulia Kashirina. All rights reserved.
//

#import "Friend.h"
#import "VKCCoreDataManager.h"

@implementation Friend

@dynamic name;
@dynamic userId;
@dynamic dateOfBirth;
@dynamic gender;
@dynamic photoUrl;


+ (nonnull instancetype)createFriendWithId:(nonnull NSString *)userId {
    [Friend deleteIfExistFriendWithId:userId];
    Friend *friend = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass(self) inManagedObjectContext:[VKCCoreDataManager sharedManager].managedObjectContext];
    friend.userId = userId;
    [VKCCoreDataManager.sharedManager saveContext];
    return friend;
}

+ (void)deleteIfExistFriendWithId:userId {
    NSManagedObjectContext *context = [VKCCoreDataManager sharedManager].managedObjectContext;
    Friend *friend = [self findFriendWithId:userId];
    if (friend) {
        [context deleteObject:friend];
        NSError *error = nil;
        [context save:&error];
        if (error) {
            NSLog(@"%@", error);
        }
    }
}

+ (Friend *)findFriendWithId:(NSString *)userId {
    NSManagedObjectContext *context = [VKCCoreDataManager sharedManager].managedObjectContext;
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass(Friend.class)];
    request.predicate = [NSPredicate predicateWithFormat:@"userId = %@", userId];
    NSError *error = nil;
    NSArray *friends = [context executeFetchRequest:request error:&error];
    if (error) {
        NSLog(@"%@", error);
    }
    
    return friends.firstObject;
}

+ (nonnull NSArray *)findFriendsWithOffset:(NSInteger)offset count:(NSInteger)count {
    NSManagedObjectContext *context = [VKCCoreDataManager sharedManager].managedObjectContext;
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass(Friend.class)];
    [request setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]]];
    [request setFetchLimit:count];
    [request setFetchOffset:offset];
    NSError *error = nil;
    NSArray *friends = [context executeFetchRequest:request error:&error];
    if (error) {
        NSLog(@"%@", error);
    }
    
    return friends;
}

@end
