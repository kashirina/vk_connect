//
//  main.m
//  VK_Connect
//
//  Created by Artyom Ryzhkov on 24/10/15.
//  Copyright (c) 2015 Yulia Kashirina. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VKCAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([VKCAppDelegate class]));
    }
}
