//
//  VKCServerManager.h
//  VK_Connect
//
//  Created by Artyom Ryzhkov on 24/10/15.
//  Copyright (c) 2015 Yulia Kashirina. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <VKSdk/VKSdk.h>
@class Friend;

@protocol VKCServerManagerDelegate <NSObject>
@required
- (void)startWorking;
@end

@interface VKCServerManager : NSObject

+ (VKCServerManager *)sharedManager;

@property (strong, nonatomic) id <VKCServerManagerDelegate> delegate;

- (BOOL)isUserAuthorized;

- (void)authorize;
- (void)logout;

- (void)getFriendsWithOffset:(NSInteger)offset
                        count:(NSInteger)count
                   onSuccess:(void(^)(NSArray <Friend *> *friends))success
                   onFailure:(void(^)(NSError *error))failure;

@end
