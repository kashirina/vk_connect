//
//  VKCServerManager.m
//  VK_Connect
//
//  Created by Artyom Ryzhkov on 24/10/15.
//  Copyright (c) 2015 Yulia Kashirina. All rights reserved.
//

#import "VKCServerManager.h"
#import "VKCCoreDataManager.h"
#import <VKSdk/VKSdk.h>
#import "Friend.h"

@interface VKCServerManager () <VKSdkDelegate>
@end

@implementation VKCServerManager

+ (VKCServerManager *)sharedManager {
    static VKCServerManager *manager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[VKCServerManager alloc] init];
        [VKSdk initializeWithDelegate:manager andAppId:@"5118819"];
    });
    
    return manager;
}

- (BOOL)isUserAuthorized {
    return [VKSdk isLoggedIn];
}

- (void)authorize {
    [VKSdk authorize:@[VK_PER_FRIENDS] revokeAccess:YES];
}

- (void)logout {
    [VKSdk forceLogout];
}

- (void)getFriendsWithOffset:(NSInteger)offset
                        count:(NSInteger)count
                   onSuccess:(void(^)(NSArray <Friend *> *friends))success
                   onFailure:(void(^)(NSError *error))failure {
    NSDictionary *parameters = @{@"order":@"name",
                                 @"count":@(count),
                                 @"offset":@(offset),
                                 @"fields":@"photo_50,sex,bdate",
                                 @"name_case":@"nom"};
    
    VKRequest *friendsReqest =[[VKApi friends] get:parameters];
    [friendsReqest executeWithResultBlock:^(VKResponse *response) {
        NSArray *friendsArray = [response.json valueForKey:@"items"];
        NSMutableArray *friends = [NSMutableArray array];
        for (NSDictionary *friendDict in friendsArray) {
            Friend *friend = [Friend createFriendWithId:[friendDict[@"id"] stringValue]];
            friend.name = [NSString stringWithFormat:@"%@ %@",
                           friendDict[@"first_name"],
                           friendDict[@"last_name"]];
            friend.gender = ([friendDict[@"sex"] isEqual:@1]) ? @"женский" : @"мужской";
            friend.photoUrl = friendDict[@"photo_50"];
            friend.dateOfBirth = friendDict[@"bdate"];
            [VKCCoreDataManager.sharedManager saveContext];
            [friends addObject:friend];
        }
        if (success) {
            success(friends);
        }
    }
    errorBlock:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

#pragma mark - VKSdkDelegate

- (void)vkSdkShouldPresentViewController:(UIViewController *)controller {
    UIViewController *rootVC = [[[UIApplication sharedApplication] keyWindow] rootViewController];
    if ([rootVC isKindOfClass:[UINavigationController self]]) {
        UINavigationController *nvc = (id)rootVC;
        [nvc.topViewController presentViewController:controller animated:YES completion:nil];
    }
}

- (void)vkSdkTokenHasExpired:(VKAccessToken *)expiredToken {
    [self authorize];
}

- (void)vkSdkReceivedNewToken:(VKAccessToken *)newToken {
    [self.delegate startWorking];
}

- (void)vkSdkUserDeniedAccess:(VKError *)authorizationError {
    
}

- (void)vkSdkNeedCaptchaEnter:(VKError *)captchaError {
    VKCaptchaViewController *vc = [VKCaptchaViewController captchaControllerWithError:captchaError];
    UIViewController *rootVC = [[[UIApplication sharedApplication] keyWindow] rootViewController];
    if ([rootVC isKindOfClass:[UINavigationController self]]) {
        UINavigationController *nvc = (id)rootVC;
        [vc presentIn:nvc.topViewController];
    }
}

@end
