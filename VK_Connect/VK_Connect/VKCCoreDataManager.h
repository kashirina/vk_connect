//
//  CoreDataManager.h
//  VK_Connect
//
//  Created by Artyom Ryzhkov on 25/10/15.
//  Copyright © 2015 Yulia Kashirina. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface VKCCoreDataManager : NSObject

+ (VKCCoreDataManager *)sharedManager;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

- (void)removeAllData;

@end
