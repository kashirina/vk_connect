//
//  ViewController.h
//  VK_Connect
//
//  Created by Artyom Ryzhkov on 24/10/15.
//  Copyright (c) 2015 Yulia Kashirina. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VKCFriendsViewController : UITableViewController

- (void)checkLogin;

@end

