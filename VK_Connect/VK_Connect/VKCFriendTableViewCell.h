//
//  VKCFriendTableViewCell.h
//  VK_Connect
//
//  Created by Artyom Ryzhkov on 25/10/15.
//  Copyright © 2015 Yulia Kashirina. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VKCFriendTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *genderLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateOfBirthLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@end
